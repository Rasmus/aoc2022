#!/usr/bin/env python3

space = {"/":0}
loc = ["/"]

for c in open("07/i").read().split("$"):
  if c[0:6]==" cd ..":
    loc.pop()
  elif c[0:5]==" cd /":
    loc = ["/"]
  elif c[0:4]==" cd ":
    loc.append(c.strip().split(" ")[1])

  elif c[0:3]==" ls":
    for l in c.strip().split("\n")[1:]:
      txloc = "/".join(loc) if len(loc)>1 else "/"

      if l[0:3]=="dir":
        new_dir = "/".join([txloc,l.split(" ")[1]])
        space[new_dir] = 0

      else:
        val = int(l.split(" ")[0])
        space[txloc] += val
        for i in range(1,len(loc)):
          space["/".join(loc[:i])]+=val

print(sum(filter(lambda x: x<100000,space.values())))
print(sorted([(v, k) for k, v in space.items() if v > 30000000-(70000000-space["/"])])[0][0])
