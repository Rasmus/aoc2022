#!/usr/bin/env python3

import numpy as np
a = np.array([list(map(int,i)) for i in [list(j) for j in open("08/i").read().strip().split("\n")]])
visible = np.shape(a)[0]*2+(np.shape(a)[1]-2)*2

for y in range(1,np.shape(a)[0]-1):
  for x in range(1,np.shape(a)[1]-1):
    # Visible?
    left  = True if np.where(a[y][0:x]>=a[y][x])[0].size<1 else False
    right = True if np.where(a[y][x:] >=a[y][x])[0][1:].size<1 else False
    aktiv = True if np.where(a[:,x][0:y]>=a[y,x])[0].size<1 else False
    passiv =True if np.where(a[:,x][y:]>=a[y,x])[0][1:].size<1 else False
    
    if left or right or aktiv or passiv:
      visible+=1

print(visible)