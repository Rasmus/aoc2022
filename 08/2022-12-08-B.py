#!/usr/bin/env python3

import numpy as np

a = np.array([list(map(int,i)) for i in [list(j) for j in open("08/i").read().strip().split("\n")]])

best = 0

for y in range(1,np.shape(a)[0]-1):
  for x in range(1,np.shape(a)[1]-1):
    left  = x if len(list(np.where(a[y][0:x]>=a[y][x])[0]))<1 else x-max(list(np.where(a[y][0:x]>=a[y][x])[0]))
    right = np.shape(a)[1]-(x+1) if len(list(np.where(a[y][x:]>=a[y][x])[0][1:]))<1 else min(list(np.where(a[y][x:]>=a[y][x])[0][1:]))
    aktiv = y if len(list(np.where(a[:,x][0:y]>=a[y,x])[0]))<1 else y-max(list(np.where(a[:,x][0:y]>=a[y,x])[0]))
    passiv = np.shape(a)[0]-(y+1) if len(list(np.where(a[:,x][y:]>=a[y,x])[0][1:]))<1 else min(list(np.where(a[:,x][y:]>=a[y,x])[0][1:]))

    if left*right*aktiv*passiv>best: best=left*right*aktiv*passiv
    
print(best)
