#!/usr/bin/env python3

a=open("06/i").read()

for x in [4,14]:
    for i in range(x,len(a)):
        test_str = a[i-x:i]
        fail = False
        for c in test_str:
            if test_str.count(c) > 1:
                fail = True
        if not fail:
            print(i)
            print(test_str)
            break