#!/usr/bin/env python3

a=open("06/i").read()

for x in [4,14]:
    for i in range(x,len(a)):
        if sum([a[i-x:i].count(y) for y in a[i-x:i]]) == x:
            print(i)
            break