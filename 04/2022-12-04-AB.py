#!/usr/bin/env python3

result = [0,0]
import re 
for l in open("04/i").readlines():
    a = list(map(int,re.split("[,-]",l.strip())))
    b = len(set(range(a[0],a[1]+1)).intersection(range(a[2],a[3]+1)))
    if b == len(range(a[0],a[1]))+1 or b == len(range(a[2],a[3]))+1:
        result[0]+=1
        result[1]+=1
    elif b != 0:
        result[0]+=1

print(result)