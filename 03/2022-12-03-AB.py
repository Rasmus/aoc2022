#!/usr/bin/env python3

# The long way round
def valuar(instr):
    return ord(instr)-38 if instr.isupper() else ord(instr)-96

result = 0

for l in open("03/i"):
    common = list(set(l[0:int(len(l)/2)]).intersection(l[int(len(l)/2):]))[0]
    result += valuar(common)

print(result)

result = 0

for team in more_itertools.chunked(open("03/i").read().splitlines(),3):
    shared = list(set.intersection(set(team[0]),set(team[1]),set(team[2])))[0]
    result += valuar(shared)

print(result)