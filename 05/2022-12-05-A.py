#!/usr/bin/env python3

import numpy as np
import re

piles = []

a = open("05/i").read().split("\n\n")
b = list(reversed(a[0].split("\n")))

for l in b:
    temp = []
    for i in range(0,len(l),4):
        temp.append(l[i+1].strip())
    piles.append(temp)

piles=[[i for i in x if i] for x in np.transpose(piles)]

for l in a[1].strip().split("\n"):
    c=list(map(int,re.search("move (\d+) from (\d+) to (\d+)",l).groups()))
    for i in range(0,c[0]):
        piles[c[2]-1].append(piles[c[1]-1].pop())

d = [i.pop() for i in piles]
print(d)