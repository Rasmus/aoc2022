#!/usr/bin/env python3

a="""R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2"""
a=open("/Users/rasmus/Gits/aoc2022/09/i").read().strip()

b={"L":-1, "U":1, "R":1, "D":-1}
head=[(10,10)]
tail=[(10,10)]

def is_close(objA,objB):
  return abs(objA[0]-objB[0])<2 and abs(objA[1]-objB[1])<2

def mov(cmd,obj):
  steps=int(cmd[2:])+1
  result=[]
  if cmd[0] == "U":
    for i in range(1,steps):
      result.append((obj[0],obj[1]+i))
  elif cmd[0] == "D":
    for i in range(1,steps):
      result.append((obj[0],obj[1]-i))
  elif cmd[0] == "R":
    for i in range(1,steps):
      result.append((obj[0]+i,obj[1]))
  else:
    for i in range(1,steps):
      result.append((obj[0]-i,obj[1]))
  print("cmd: {} result: {}".format(cmd,result))
  return result

counter = 1
for l in a.split("\n"):
  print(f"\nNew line ({counter})")
  counter+=1
  head_start = head[-1]
  #dia_jump = mov(l,head_start)[-2]
  #print(f"-{dia_jump}-")
  #break
  print(f"head start: {head_start}")
  head_move = mov(l,head_start)
  head.extend(head_move)

  tpos=tail[-1]
  hpos=head[-1]
  #print(is_close(tpos,hpos))
  #continue
  #print(head_start)
  print(f"HS {head_start} TP {tpos} HP {hpos}")

  # Diagonalt.
  if is_close(hpos,tpos):
    print("Is close")
    tail.append(tpos)
    continue
  
  if hpos[1]!=tpos[1] and tpos[0]!=hpos[0]:
    print(f"Udfordringer. {head_move} tpos: {tpos} hpos: {hpos} hs: {head_start}")
    temp = []
    all_head_move = [head_start]
    all_head_move.extend(head_move)
    for move in all_head_move:
      if is_close(move,tpos):
        temp = move 
      else:
        break
    if temp == []:
      print(f"All is lost.\nhpos: {hpos}\ntpos: {tpos}\nhead_start: {head_start}\nhead_move: {head_move}")
      exit()
    tail.append(temp)
    tpos=temp
    print(f"Sat move til {temp}")

  # Same column
  if tpos[0]==hpos[0]:
    # If head above of tail
    if hpos[1]>tpos[1]:
      print("Head is above of tail {} {}".format(hpos[1],tpos[1]))
      steps = hpos[1]-tpos[1]-1
      tail.extend(mov(f"U {steps}",tpos))
    else:
      print("Head is below tail {} {}".format(hpos[1],tpos[1]))
      steps = tpos[1]-hpos[1]-1
      tail.extend(mov(f"D {steps}",tpos))

  # Same rpw
  elif tpos[1]==hpos[1]:
    print(f"Same row. tpos: {tpos} hpos: {hpos}")
    if hpos[0]>tpos[0]:
      steps = hpos[0]-tpos[0]-1
      print(f"Steps (hpos>tpos) {steps}")
      tail.extend(mov(f"R {steps}",tpos))
      #print(mov(f"R {steps}",tpos))
    else:
      steps = tpos[0]-hpos[0]-1
      print(f"Steps hpos<tpos {steps}")
      tail.extend(mov(f"L {steps}",tpos))
      #print(mov(f"L {steps}",tpos))

print(head)
print(tail)
print(len(sorted(set(tail))))