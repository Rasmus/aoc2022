#!/usr/bin/env python3

with open("01/2022-12-01-input.txt", "r") as f:
    elves = [x.split("\n") for x in f.read().split("\n\n")]
    ints = [[int(j) for j in i] for i in elves]
    sums = sorted([sum(i) for i in ints])
    
    print(sums[-1])
