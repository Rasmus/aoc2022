#!/usr/bin/env python3

# Readable
with open("01/2022-12-01-input.txt") as f:
    elves_calories = [[int(j) for j in i] for i in [x.split("\n") for x in f.read().split("\n\n")]]
    print(sorted([sum(x) for x in elves_calories])[-1])
    print(sum(sorted([sum(x) for x in elves_calories])[-3:]))

# One-liner
with open("01/2022-12-01-input.txt") as f: print(sorted([sum(x) for x in [[int(j) for j in i] for i in [x.split("\n") for x in f.read().split("\n\n")]]])[-1])